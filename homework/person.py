'''Zad.1
Chciałbym aby po wykonaniu
`print(person)`
wyświetliły się dane z dowodu (imię, nazwisko, pesel, nr.dowodu).
Człowiek może zawierać tylko 1 zmienną.
'''


# Zad.1
class Person:
    def __init__(self, information):
        self.personalData = PersonalData(information)

    def __repr__(self):
        return self.personalData.__repr__()


class PersonalData:
    def __init__(self, information):
        self.information = information
        list = self.information.split()
        self.firstName = list[0]
        self.surname = list[1]
        self.pesel = list[2]
        self.ID = list[3]

    def __repr__(self):
        return f'{self.firstName} {self.surname} \nPESEL: {self.pesel} \nID: {self.ID}'


if __name__ == "__main__":
    person = Person('Jan Kowalski 80120713555 ARW5656955')
    print(person)
