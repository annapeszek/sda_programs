'''Zad. 2
Stwórzmy sobie symulację jazdy samochodu po drodze
sprawdźmy na którym znaku ograniczenia prędkości
 samochód powinien dostać mandat.
Samochód od początku porusza się ze stałą prędkością.
W momencie przekroczenia prędkości samochód nie jedzie dalej oraz
jest informacja jaka marka samochodu przekroczyła prędkość
oraz na jakim znaku (wyświetlić unikalny identyfikator oraz maksymalną wartość prędkości)

'''
#Zad.2 Symulator jazdy
import random
import string


class Car:
    def __init__(self, mark):
        self.mark = mark

    def go(self, speed, way):
        roadSign = way.popRoadSign()
        while speed <= roadSign.speedLimit:
            if way.roadSignList == []:
                print('You are at the end of the road!')
                break
            roadSign = way.popRoadSign()

        if speed > roadSign.speedLimit:
            print(f'''\nYou was driving too fast! \nYou got speeding ticket!
        
        ****
        Car mark: {c.mark}
        Car speed: {speed} km/h
        Speed limit: {roadSign.speedLimit} km/h
        Road Sign ID: {roadSign.ID}
        Fine: 500 zł
        ***
        ''')


class RoadSign:
    def __init__(self, speedLimit):
        self.speedLimit = speedLimit
        text = ''.join(random.choice(string.ascii_letters) for i in range(5))
        self.ID = str(random.randrange(1000, 9000)) + text

    def __repr__(self):
        return self.ID


class Road:
    def __init__(self):
        self.roadSignList = []
        for i in range(10):
            self.roadSignList.append(RoadSign(100 - i * 10))

    def popRoadSign(self):
        return self.roadSignList.pop(0)

    def __repr__(self):
        for sign in self.roadSignList:
            print(sign)


if __name__ == '__main__':
    c = Car('Fiat')
    w = Road()
    speed = (int(input('How fast do you want to go? [km/h]: ')))
    c.go(speed, w)


