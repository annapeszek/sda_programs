'''Tworzymy mały interfejsik dla gry
w której są różne characters (postacie).
Napisałem tutaj taki mały test do tego zadania,
takie wyniki powinny się znaleźć pod danymi wywołaniami.
PODPOWIEDŹ: jeśli do jakiejś zmiennej nie będziecie mogli się dostać
to użyjcie super z odpowiednim konstruktorem :) '''

'''
hp - punkty życia
mp - punkty magii

Kod:
'''

# Zad.4 Characters

from abc import ABC, abstractmethod


class Character(ABC):
    def __init__(self):
        self.hp = 50
        self.mp = 50

    @abstractmethod
    def attack(self):
        pass


class Magic(Character):
    def attack(self):
        self.mp -= 10


class Warrior(Character):
    def attack(self):
        pass


if __name__ == '__main__':
    # character = Character()
    magic = Magic()
    warrior = Warrior()

    # print(character.hp)  # 50
    # print(character.mp)  # 50
    # character.attack()
    # print(character.hp)  # 50
    # print(character.mp)  # 50

    print('magic:')
    print(magic.hp)  # 50
    print(magic.mp)  # 50
    magic.attack()
    print(magic.hp)  # 50
    print(magic.mp)  # 40

    print('warrior:')
    print(warrior.hp)  # 50
    print(warrior.mp)  # 50
    warrior.attack()
    print(warrior.hp)  # 50
    print(warrior.mp)  # 50
