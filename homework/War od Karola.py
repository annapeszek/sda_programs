#gra w wojnę
import random

cardColors = ["Spade", "Club", "Heart", "Diamond"]
cardFigures = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']
cardValue = list(range(2, 15))
cardsOnTable = []


class Card:
    def __init__(self, colour, figure, value):
        self.colour = colour
        self.figure = figure
        self.value = value

    def __repr__(self):
        return f'{self.figure} {self.colour},  Value: {self.value}'


class CardList:
    def __init__(self):
        self.cardList = []
        for colour in cardColors:
            for figure in cardFigures:
                i = cardFigures.index(figure)
                self.cardList.append(Card(colour, figure, cardValue[i]))

    def __repr__(self):
        for card in self.cardList:
            print(card)

    def shuffleCards(self):
        random.shuffle(self.cardList)

    def popCard(self):
        return self.cardList.pop()


class Player:
    def __init__(self, name):
        self.name = name
        self.playersCards = []

    def __repr__(self):
        for card in self.playersCards:
            print(card)

    def giveCard(self):
        return self.playersCards.pop(0)


class Table:

    def __init__(self):
        tableCardList = CardList()
        tableCardList.shuffleCards()
        self.player1 = Player('Player1')
        self.player2 = Player('Player2')
        self.winner = ''

        counter = 0
        for i in range(4):#52
            if counter % 2:
                self.player1.playersCards.append(tableCardList.popCard())
            else:
                self.player2.playersCards.append(tableCardList.popCard())
            counter += 1

        while (self.player1.playersCards != [] and self.player2.playersCards != []):
            self.play()

    def play(self):
        print('Cards on table:')
        global cardsOnTable
        card1 = self.player1.giveCard()
        card2 = self.player2.giveCard()
        cardsOnTable.append(card1)
        cardsOnTable.append(card2)
        print(card1)
        print(card2)
        print(len(self.player1.playersCards))
        print(len(self.player2.playersCards))
        if card1.value > card2.value:
            self.player1.playersCards.extend(cardsOnTable)
            cardsOnTable = []
            print(self.player1.name, 'win!!!')
            self.winner = self.player1.name

        elif card1.value < card2.value:
            self.player2.playersCards.extend(cardsOnTable)
            cardsOnTable = []
            print(self.player2.name, 'win!!!')
            self.winner = self.player2.name

        else:
            print('card have the same value')

            if self.player1.playersCards == []:
                self.winner = self.player2.name
                return
            elif self.player2.playersCards == []:
                self.winner = self.player1.name
                return
        random.shuffle(self.player2.playersCards)
        random.shuffle(self.player1.playersCards)
#            else:
#                self.play(self.player1, self.player2)


if __name__ == '__main__':
    t = Table()
    print('\nWinner:')
    print(t.winner)
