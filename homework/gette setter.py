'''
Zad 3.
Stwórzmy sobie klasę Grade która zawiera __score, score może być tylko w zakresie 0 - 100
Jeśli jest poza zakresem to należy wyprintować jakąś informację o tym
Za pomocą property utwórzcie poprawny getter i setter oraz przykladowe wywołanie'''

#Zad.3
class Grade:
    def __init__(self):
        self.__score = 0

    @property
    def score(self):
        pass

    @score.setter
    def score(self, points):
        if 0 <= points <= 100:
            self.__score = points
        else:
            return print('Score is out of range')

    @score.getter
    def score(self):
        return self.__score


if __name__ == '__main__':
    g = Grade()
    print(g.score)
    g.score = 50
    print(g.score)
    g.score = 150
