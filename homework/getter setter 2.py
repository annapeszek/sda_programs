# Zad 3.
# Stwórzmy sobie klasę Grade która zawiera __score, score może być tylko w zakresie 0 - 100
# Jeśli jest poza zakresem to należy wyprintować jakąś informację o tym
# Za pomocą property utwórzcie poprawny getter i setter oraz przykladowe wywołanie

import time


class Grade:
    def __init__(self, score=0):
        self._score = score

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, points):
        if points > 100 or points < 0:
            raise ValueError('Score number must be within range 0-100')
        self._score = points


gameResult1 = Grade(32)
print(gameResult1.score)
gameResult1.score = 64
print(gameResult1.score)
try:
    gameResult1.score = 101
except ValueError:
    print("ValueError")
except:
    print("SOMETHING")

print(gameResult1.score)
time.sleep(3)
gameResult1.score = 50
print(gameResult1.score)
