#gra w wojnę
import random

class Card:
    def __init__(self, colour, figure, value):
        self.colour = colour
        self.figure = figure
        self.value = value

    def __repr__(self):
        return f'{self.figure} {self.colour},  Value: {self.value}'


class CardList:
    def __init__(self):
        cardColors = ["Spade", "Club", "Heart", "Diamond"]
        cardFigures = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']
        cardValue = list(range(2, 15))
        self.cardList = []
        for colour in cardColors:
            for figure in cardFigures:
                i = cardFigures.index(figure)
                self.cardList.append(Card(colour, figure, cardValue[i]))

    def __repr__(self):
        for card in self.cardList:
            print(card)

    def shuffleCards(self):
        random.shuffle(self.cardList)

    def popCard(self):
        return self.cardList.pop(0)


class Player:
    def __init__(self, name):
        self.name = name
        self.playersCards = []

    # def __repr__(self):
    #     for card in self.playersCards:
    #         print(card)

    def giveCard(self):
        return self.playersCards.pop(0)


class Table:

    def __init__(self):
        tableCardList = CardList()
        tableCardList.shuffleCards()
        player1 = Player('Player1')
        player2 = Player('Player2')
        self.winner = ''
        self.cardsOnTable = []

        counter = 0
        for i in range(4): #52
            if counter % 2:
                player1.playersCards.append(tableCardList.popCard())
            else:
                player2.playersCards.append(tableCardList.popCard())
            counter += 1
        print(player1.playersCards)
        print(player2.playersCards)

        while (player1.playersCards != [] and player2.playersCards != []):
            self.play(player1, player2)

    def play(self, player1, player2):
        print('Cards on table:')

        card1 = player1.giveCard()
        card2 = player2.giveCard()
        self.cardsOnTable.append(card1)
        self.cardsOnTable.append(card2)
        print(card1)
        print(card2)
        if card1.value > card2.value:
            player1.playersCards.extend(self.cardsOnTable)
            self.cardsOnTable = []
            print(player1.name, 'win!!!')
            self.winner = player1.name

        elif card1.value < card2.value:
            player2.playersCards.extend(self.cardsOnTable)
            self.cardsOnTable = []
            print(player2.name, 'win!!!')
            self.winner = player2.name

        else:
            print('card have the same value')

            if player1.playersCards == []:
                self.winner = player2.name
                return
            elif player2.playersCards == []:
                self.winner = player1.name
                return
            # else:
            #     self.play(player1, player2)


if __name__ == '__main__':
    t = Table()
    print('\nWinner:')
    print(t.winner)
